import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppAuthGuardService as AuthGuard } from './common/utility/auth-guard.service';
import { TAPolicyComponent } from './mqProvider/policy/policy.component';
import { TATermsConditionComponent } from './mqProvider/terms-condition/terms-condition.component';
import { TAWorkSpaceComponent } from './mqProvider/workspace/workspace.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Login',
    },
    loadChildren: () =>
      import('./mqProvider/authentication/authentication.module').then(
        (module) => module.MQPAuthenticationModule
      ),
  },
  {
    path: 'mqp',
    component: TAWorkSpaceComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        data: {
          title: 'Home',
        },
        loadChildren: () =>
          import('./mqProvider/home/home.module').then((m) => m.MQPHomeModule),
      },
      {
        path: 'policy',
        data: {
          title: 'Our Policy',
        },
        component: TAPolicyComponent,
      },
      {
        path: 'terms',
        data: {
          title: 'Terms & Conditions',
        },
        component: TATermsConditionComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
