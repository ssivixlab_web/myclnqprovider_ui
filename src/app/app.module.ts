import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TACommonModule } from './common/common.module';
import { TAMessageNotificationService } from './common/message-notification/messageNotification.service';
import { TAAppErrorService } from './common/utility/app-error.service';
import { TAPolicyComponent } from './mqProvider/policy/policy.component';
import { TATermsConditionComponent } from './mqProvider/terms-condition/terms-condition.component';
import { TAWorkSpaceComponent } from './mqProvider/workspace/workspace.component';

@NgModule({
  declarations: [
    AppComponent,
    TAWorkSpaceComponent,
    TAPolicyComponent,
    TATermsConditionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TACommonModule,
  ],
  providers: [TAMessageNotificationService, TAAppErrorService],
  bootstrap: [AppComponent],
})
export class AppModule {}
