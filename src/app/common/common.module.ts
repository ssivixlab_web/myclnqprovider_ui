import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AppMaterialModule } from './app-material.module';
import { ConfirmModalModule } from './confirmModal/confirmModal.module';
import { LoaderModule } from './loader/loader.module';
import { MsgNotModule } from './message-notification/messageNotification.module';
import { AppInterceptService } from './utility/intercept.service';
import { SafePipe } from './utility/safe.pipe';

@NgModule({
  declarations: [SafePipe],
  imports: [CommonModule, LoaderModule, MsgNotModule, ConfirmModalModule],
  exports: [
    CommonModule,
    LoaderModule,
    AppMaterialModule,
    FormsModule,
    MsgNotModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    SafePipe,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptService,
      multi: true,
    },
  ],
})
export class TACommonModule {}
