import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface confirmDialog {
    dialogTitle: string;
    dialogContent: string;
    dialogPrimaryBtnText: string;
    dialogSecondaryBtnText: string;
    dialogFeatureName: string;
    dialogData?: any;
}

export interface confirmDialogAction {
    dialogFeatureName: string;
    btnConfirm: boolean;
    dialogData: any;
}

@Component({
    template: `
    <h2 class="mb-0" mat-dialog-title>{{data.dialogTitle}}</h2>
    <div mat-dialog-content>{{data.dialogContent}}</div>
    <div mat-dialog-actions>
        <button mat-button mat-dialog-close (click)="clickEvent(true)">{{data.dialogPrimaryBtnText}}</button>
        <button mat-button mat-dialog-close (click)="clickEvent(false)">{{data.dialogSecondaryBtnText}}</button>
    </div>
    `,
    encapsulation: ViewEncapsulation.None

})
export class ConfirmModalComponent {
    constructor(
        public dialogRef: MatDialogRef<ConfirmModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: confirmDialog) { }

    clickEvent(proceed: boolean) {
        const outData: confirmDialogAction = {
            dialogFeatureName: this.data.dialogFeatureName,
            btnConfirm: proceed,
            dialogData: this.data.dialogData
        };
        this.dialogRef.close(outData);
    }
}
