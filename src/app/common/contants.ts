import { environment } from 'src/environments/environment';

export const API_END_POINT_URL = environment.endPointUrl;
// All get/post/put/delete End point
export const API_URL = {
  LOGOUT: 'clinic/logOut',
  CLINIC_LOGIN: 'clinic/create',
  OTP_VERIFY: 'clinic/verifyotp',
  CLINIC_PROFILE: 'clinic/profile',
  CLINIC_NEXT_DAY: 'clinic/next/working/day',
  CLINIC_DR_LIST: 'clinic/getSelectedClinicDoctorsListForHomeScreen',
};

export const STATIC_BASE_API_URL = {
  PRIVACY: 'https://ssivixlab.com/privacypolicy.htm',
  TERMSCONDITION: 'https://ssivixlab.com/termsandcondition.html',
};

// Session, Header, Localstorage related constants
export const SESSION_STORAGE_KEY = {
  SESSION_ID: 'SSIVIXLAB_PROVIDER_SESSION_INFO',
  CLINIC_ID: 'SSIVIXLAB_PROVIDER_CLINIC_ID',
  CLINIC_INFO: 'SSIVIXLAB_PROVIDER_CLINIC_INFO',
  CLINIC_PROFILE: 'SSIVIXLAB_PROVIDER_CLINIC_PROFILE',
};

export const NO_INTERCEPT_LOADER = 'NO__LOADER';
