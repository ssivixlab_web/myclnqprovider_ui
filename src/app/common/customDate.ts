import { NativeDateAdapter } from '@angular/material/core';

export class CustomDateAdapter extends NativeDateAdapter {

    format(date: Date, displayFormat: string): string {
        if (displayFormat === 'DD-MM-YYYY') {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();

            return `${day}-${month}-${year}`;
        }
        return date.toDateString();
    }
}
