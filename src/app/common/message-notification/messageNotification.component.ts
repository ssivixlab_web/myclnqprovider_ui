import {
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  ImessageData,
  TAMessageNotificationService,
} from './messageNotification.service';
interface Alert {
  type: string;
  message: string;
}
@Component({
  selector: 'ta-message-notification',
  templateUrl: './messageNotification.component.html',
  styleUrls: ['./messageNotification.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TAMessageNotificationComponent implements OnInit {
  dismissible = true;
  alert: ImessageData[] = [];
  constructor(
    private getMsg: TAMessageNotificationService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.getMsg.getMsg();
    this.getMsg.getMsg().subscribe((res) => {
      console.log('res', res);
      this.alert = res;
      this.ref.detectChanges();
    });
  }

  onClosed(alert: Alert) {
    this.alert = [];
  }
}
