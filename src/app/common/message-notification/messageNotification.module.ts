import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TAMessageNotificationComponent } from './messageNotification.component';

@NgModule({
  declarations: [TAMessageNotificationComponent],
  imports: [CommonModule, AlertModule.forRoot()],
  exports: [TAMessageNotificationComponent],
  providers: [],
})
export class MsgNotModule {}
