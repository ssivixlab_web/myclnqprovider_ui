import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AppAuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AppAuthGuardService implements CanActivate, CanLoad {
  constructor(public router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (!AppAuthenticationService.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  canLoad(): boolean {
    // const a = AppAuthenticationService.getByKey(SESSION_STORAGE_KEY.SESSION_ID);
    // if (a.isLoggedIn && !a.isProfileAvailable) {
    //   this.router.navigateByUrl('');
    //   return false;
    // }
    return true;
  }
}
