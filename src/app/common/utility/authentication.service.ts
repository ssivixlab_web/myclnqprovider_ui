import { Injectable } from '@angular/core';
import { SESSION_STORAGE_KEY } from '../contants';

@Injectable()
export class AppAuthenticationService {
  // check for session
  public static isAuthenticated(): boolean {
    const a =
      sessionStorage[SESSION_STORAGE_KEY.SESSION_ID] &&
      sessionStorage[SESSION_STORAGE_KEY.CLINIC_ID]
        ? true
        : false;
    return a;
  }

  public static setSessionStorage(sessionKey: string, sessionValue: string) {
    sessionStorage[sessionKey] = sessionValue;
  }

  public static getByKey(key: string): any {
    if (sessionStorage[key] && sessionStorage[key] !== '') {
      return JSON.parse(sessionStorage[key]);
    }
    return '';
  }

  public static removeSessionStorage() {
    sessionStorage.removeItem(SESSION_STORAGE_KEY.SESSION_ID);
    sessionStorage.removeItem(SESSION_STORAGE_KEY.CLINIC_ID);
    sessionStorage.removeItem(SESSION_STORAGE_KEY.CLINIC_INFO);
    sessionStorage.removeItem(SESSION_STORAGE_KEY.CLINIC_PROFILE);
  }
}
