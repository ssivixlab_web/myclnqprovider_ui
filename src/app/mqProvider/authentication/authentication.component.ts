import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MQPAuthenticationComponent implements OnInit {
  constructor() {}
  images = [
    {
      img: 'receiver_walkthrough_1.png',
      text: 'MANAGE APPOINTMENTS',
      subtext: 'Manage patient appointment more easier than ever',
    },
    {
      img: 'receiver_walkthrough_2.png',
      text: 'EMERGENCY SLOT',
      subtext: 'Create & assign emergency slots at ease',
    },
    {
      img: 'receiver_walkthrough_3.png',
      text: 'MANAGE DOCTORS',
      subtext: 'Update doctor profile and manage availability',
    },
  ];
  ngOnInit() {}
}
