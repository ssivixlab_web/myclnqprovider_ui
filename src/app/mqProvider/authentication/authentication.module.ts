import { NgModule } from '@angular/core';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { TACommonModule } from 'src/app/common/common.module';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { MQPAuthenticationComponent } from './authentication.component';
import { MQPAuthRoutingModule } from './authentication.routing.module';
import { MQPAuthService } from './authentication.service';
import { MQPLoginComponent } from './login/login.component';
import { MQPMobileComponent } from './mobile/mobile.component';
import { MQPOtpComponent } from './otp/otp.component';

@NgModule({
  declarations: [
    MQPAuthenticationComponent,
    MQPLoginComponent,
    MQPMobileComponent,
    MQPOtpComponent,
  ],
  imports: [
    MQPAuthRoutingModule,
    TACommonModule,
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    NgxIntlTelInputModule,
  ],
  providers: [AppAuthenticationService, MQPAuthService],
})
export class MQPAuthenticationModule {}
