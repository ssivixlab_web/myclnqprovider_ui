import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MQPAuthenticationComponent } from './authentication.component';
import { MQPLoginComponent } from './login/login.component';

export const TAhomeRoutes: Routes = [
  {
    path: '',
    component: MQPAuthenticationComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: MQPLoginComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(TAhomeRoutes)],
  exports: [RouterModule],
})
export class MQPAuthRoutingModule {}
