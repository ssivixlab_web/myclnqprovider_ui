import { Injectable } from '@angular/core';
import { API_URL, SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable({
  providedIn: 'root',
})
export class MQPAuthService {
  constructor(private httpApi: AppHttpApiService) {}

  public mobileVerification(mobileverify) {
    return this.httpApi.postData(API_URL.CLINIC_LOGIN, mobileverify);
  }

  public otpVerification(otpVerify) {
    return this.httpApi.postData(API_URL.OTP_VERIFY, otpVerify);
  }

  public setSessionData(UserData) {
    AppAuthenticationService.setSessionStorage(
      SESSION_STORAGE_KEY.SESSION_ID,
      JSON.stringify({ session: UserData.session })
    );
    AppAuthenticationService.setSessionStorage(
      SESSION_STORAGE_KEY.CLINIC_ID,
      JSON.stringify({ clinicid: UserData.clinic._id })
    );
    AppAuthenticationService.setSessionStorage(
      SESSION_STORAGE_KEY.CLINIC_INFO,
      JSON.stringify({ clinicid: UserData.clinic })
    );
  }

  public getClinicProfile(clinicInfo) {
    return this.httpApi.postData(API_URL.CLINIC_PROFILE, clinicInfo);
  }
}
