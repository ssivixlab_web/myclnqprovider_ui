import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { TAAppErrorService } from 'src/app/common/utility/app-error.service';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
import { UtilityService } from '../../common/utility.service';
import { MQPAuthService } from '../authentication.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MQPLoginComponent implements OnInit {
  loginForm: FormGroup;
  errors: string = '';
  showotp: boolean = false;
  constructor(
    private authService: MQPAuthService,
    private router: Router,
    private err: TAAppErrorService
  ) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      mobileNumber: new FormControl('', Validators.required),
    });
  }

  phoneNumberCheck(payload): void {
    const phonePayload = {
      countryCode: payload.mobileNumber.dialCode.replace(/\+/g, ''),
      mobileNumber: payload.mobileNumber.number.replace(/\s/g, ''),
    };
    this.authService.mobileVerification(phonePayload).subscribe((res) => {
      if (
        res &&
        res.status &&
        (res.accStatus === 'LOGIN' || res.accStatus === 'NEW_ACCOUNT')
      ) {
        this.showOTPandAddControl();
      } else if (res.error_code) {
        alert('Please enter valid information');
      }
    });
  }

  showOTPandAddControl() {
    this.showotp = true;
    this.loginForm.addControl(
      'otp',
      new FormControl('', [Validators.required, Validators.minLength(4)])
    );
  }

  otp(payload) {
    const otpDetails = {
      countryCode: payload.mobileNumber.dialCode.replace(/\+/g, ''),
      mobileNumber: payload.mobileNumber.number.replace(/\s/g, ''),
      otp: payload.otp,
      OSType: 'WEB',
      fcmToken: '',
      deviceId: '',
    };
    this.authService.otpVerification(otpDetails).subscribe(
      (res) => {
        this.otpVerifyCb(res);
      },
      (err) => {
        this.err.showErrormessage(err);
      }
    );
  }

  private otpVerifyCb(res) {
    if (res && res.status && res.clinic && res.session) {
      this.authService.setSessionData(res);
      this.getClinicDetails();
    } else if (res.error_code) {
      this.err.showErrormessage(res);
    }
  }

  hideOTP() {
    this.loginForm.removeControl('otp');
    this.loginForm.updateValueAndValidity();
    this.showotp = false;
  }

  getClinicDetails() {
    const clinicInfo = {
      clinicId: UtilityService.getClinicId(),
      date: UtilityService.currentDate(), //2018-06-08T11:54:08+05:30
    };
    this.authService.getClinicProfile(clinicInfo).subscribe((res) => {
      console.log('res', res);
      if (res && res.clinicDetails && res.doctorDetails) {
        const clinicprofile = {
          clinicprofile: res,
          clinicSetup: false,
          clinicTiming: false,
          clinicConfigure: false,
          addDoctor: false,
        };
        UtilityService.setItem(
          SESSION_STORAGE_KEY.CLINIC_PROFILE,
          clinicprofile
        );
        const clinicDetails = res.clinicDetails;
        const doctorDetails = res.doctorDetails;

        if (
          clinicDetails.locationName !== null &&
          clinicDetails.clinicTimings !== 0 &&
          clinicDetails.bookingConfiguration.advanceBookingLimit != null &&
          doctorDetails !== 0
        ) {
          const updateClinicprofile = {
            clinicprofile: res,
            clinicSetup: true,
            clinicTiming: true,
            clinicConfigure: true,
            addDoctor: true,
          };
          UtilityService.setItem(
            SESSION_STORAGE_KEY.CLINIC_PROFILE,
            updateClinicprofile
          );
          this.router.navigate(['mqp/home']);
        } else {
          alert('The Feature to on-board from web is currently Un-Available');
          AppAuthenticationService.removeSessionStorage();
        }
      }
    });
  }
}
