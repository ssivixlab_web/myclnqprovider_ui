import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ta-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
})
export class MQPMobileComponent implements OnInit {
  @Input() fg: FormGroup;
  constructor() {}

  ngOnInit() {}
}
