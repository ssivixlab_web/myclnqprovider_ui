import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MQPAuthService } from '../authentication.service';

@Component({
  selector: 'ta-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
})
export class MQPOtpComponent implements OnInit {
  @Input() fg: FormGroup;
  @Output() backEv = new EventEmitter();
  constructor(private authService: MQPAuthService) {}

  ngOnInit() {}

  back() {
    this.backEv.emit('hide');
  }
}
