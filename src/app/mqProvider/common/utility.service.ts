import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { SESSION_STORAGE_KEY } from 'src/app/common/contants';
import { AppAuthenticationService } from 'src/app/common/utility/authentication.service';
@Injectable()
export class UtilityService {
  constructor() {}
  public static getClinicId() {
    return AppAuthenticationService.getByKey(SESSION_STORAGE_KEY.CLINIC_ID)
      .clinicid;
  }

  public static getClinicInfo() {
    return AppAuthenticationService.getByKey(SESSION_STORAGE_KEY.CLINIC_INFO);
  }

  public static currentDate() {
    return moment().format();
  }

  public static setItem(key: string, value: any) {
    AppAuthenticationService.setSessionStorage(key, JSON.stringify(value));
  }

  public static getItem(key: string) {
    return AppAuthenticationService.getByKey(key);
  }
}
