import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { UtilityService } from '../common/utility.service';
import { MQPHomeService } from './home.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class MQPHomeComponent implements OnInit {
  listOfDoctors: any = [];
  constructor(private home: MQPHomeService) {}
  ngOnInit() {
    this.clinicHoliday();
    this.getClinicDetails();
  }

  clinicHoliday() {
    {
      const clinicInfo = {
        clinicId: UtilityService.getClinicId(),
        date: UtilityService.currentDate(),
      };
      this.home.clinicNextWorkingDay(clinicInfo).subscribe((res) => {
        if (
          res &&
          res.clinicNextWorkingDay &&
          res.clinicNextWorkingDay !== 'CLINIC_ADVANCE_BOOKING_EXCEDED'
        ) {
          const clinicWDay = res.clinicNextWorkingDay;
          var dt = new Date();
          dt.setDate(dt.getDate());
          var _dt = dt;
          const clnHld = moment(_dt).format('YYYY-MM-DD');
          if (clinicWDay === clnHld) {
            this.home.isClinicWorking = true;
          } else {
            this.home.isClinicHoliday = true;
          }
        } else if (
          (res &&
            res.clinicNextWorkingDay === 'CLINIC_ADVANCE_BOOKING_EXCEDED') ||
          res.clinicNextWorkingDay === 'CLINIC_CONTINUESLY_CLOSED'
        ) {
          this.home.isClinicHoliday = true;
        } else if (res.error_code === '10006') {
          alert('Error Occured');
        }
      });
    }
  }

  getClinicDetails() {
    const clqProfile = {
      clinicId: UtilityService.getClinicId(),
      date: UtilityService.currentDate(),
    };
    this.home.getClinicProfile(clqProfile).subscribe((res: any) => {
      if (res && res.clinicDetails && res.doctorDetails) {
        const clinicTimingsInactive =
          res.clinicDetails.clinicTimings[0].isActive;
        const doctorDetails = res.doctorDetails;
        if (!clinicTimingsInactive) {
          this.home.isclinicTimings = true;
        }
        this.getDataOfHomeScreen();
        this.home.clinicName = res.clinicDetails.clinicName;
      }
    });
  }

  getDataOfHomeScreen() {
    const clinicInfo = {
      clinicId: UtilityService.getClinicId(),
    };
    this.home.getHomeDetails(clinicInfo).subscribe((res) => {
      if (res && res.status !== 'NO DOCTORS AVAILABLE') {
        this.home.isDocAvai = true;
        this.listOfDoctors = res;
      } else if (res.status === 'NO DOCTORS AVAILABLE') {
        this.home.isDocAvai = false;
        this.listOfDoctors = [];
      }
    });
  }
}
