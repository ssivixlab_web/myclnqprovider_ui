import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TACommonModule } from 'src/app/common/common.module';
import { MQPHomeComponent } from './home.component';
import { MQPHomeService } from './home.service';

const routes: Routes = [{ path: '', component: MQPHomeComponent }];

@NgModule({
  declarations: [MQPHomeComponent],
  imports: [TACommonModule, RouterModule.forChild(routes)],
  providers: [MQPHomeService],
})
export class MQPHomeModule {}
