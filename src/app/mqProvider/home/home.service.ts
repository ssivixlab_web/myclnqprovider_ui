import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/common/contants';
import { AppHttpApiService } from 'src/app/common/utility/http-api.service';

@Injectable()
export class MQPHomeService {
  private clinicWorking: boolean = false;
  private docAvai: boolean = true;
  private clinicHoliday: boolean = false;
  private clinicTimings: boolean = false;
  private clinicNme: string = null;

  constructor(private httpApi: AppHttpApiService) {}
  get isClinicWorking(): boolean {
    return this.clinicWorking;
  }
  set isClinicWorking(value: boolean) {
    this.clinicWorking = value;
  }

  get isDocAvai(): boolean {
    return this.docAvai;
  }
  set isDocAvai(value: boolean) {
    this.docAvai = value;
  }

  get isClinicHoliday(): boolean {
    return this.clinicHoliday;
  }
  set isClinicHoliday(value: boolean) {
    this.clinicHoliday = value;
  }

  get isclinicTimings(): boolean {
    return this.clinicTimings;
  }
  set isclinicTimings(value: boolean) {
    this.clinicTimings = value;
  }

  get clinicName(): string {
    return this.clinicNme;
  }
  set clinicName(value: string) {
    this.clinicNme = value;
  }

  public clinicNextWorkingDay(clickNextDayPayload) {
    return this.httpApi.postData(API_URL.CLINIC_NEXT_DAY, clickNextDayPayload);
  }

  public getClinicProfile(clqProfile) {
    // return of(UtilityService.getItem(SESSION_STORAGE_KEY.CLINIC_PROFILE));
    return this.httpApi.postData(API_URL.CLINIC_PROFILE, clqProfile);
  }

  public getHomeDetails(clinicInfo) {
    return this.httpApi.postData(API_URL.CLINIC_DR_LIST, clinicInfo);
  }
}
