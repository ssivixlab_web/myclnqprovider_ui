import { Component, OnInit } from '@angular/core';
import { NavItem } from './sidebar';
import { SideNavService } from './sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  // public routes: typeof routes = routes;
  sideNav: NavItem[];
  public isOpenUiElements = false;

  public openUiElements() {
    this.isOpenUiElements = !this.isOpenUiElements;
  }
  constructor(private appSideNav: SideNavService) {}

  ngOnInit() {
    this.sideNav = this.appSideNav.getMenuitem();
  }
}
