import { Injectable } from '@angular/core';
import { NavItem } from './sidebar';

const MENUITEMS: NavItem[] = [
  {
    id: 10,
    displayName: 'Home',
    iconName: 'home',
    routePath: '/mqp/home',
  },
  {
    id: 20,
    displayName: 'Appointment',
    children: [
      {
        id: 21,
        displayName: 'Add New',
        iconName: 'add',
        routePath: '/mqp/addAppointment',
      },
      {
        id: 22,
        displayName: 'Manage',
        iconName: 'list_alt',
        routePath: '/mqp/viewAppointment',
      },
    ],
  },
  {
    id: 30,
    displayName: 'Chat',
    iconName: 'chat',
    routePath: '/mqp/chat',
  },
  {
    id: 50,
    displayName: 'Profile',
    iconName: 'chat',
    children: [
      {
        id: 51,
        displayName: 'Manage Clinic',
        iconName: 'add',
        routePath: '/mqp/manageClinic',
      },
      {
        id: 52,
        displayName: 'Manage Doctor',
        iconName: 'person_add',
        routePath: '/mqp/manageDoctor',
      },
      {
        id: 53,
        displayName: 'Configure Timings',
        iconName: 'access_time',
        routePath: '/mqp/configureTimings',
      },
    ],
  },
  {
    id: 50,
    displayName: 'Settings',
    children: [
      {
        id: 51,
        displayName: 'Privacy Policy',
        iconName: 'privacy_tip',
        routePath: '/mqp/policy',
      },
      {
        id: 52,
        displayName: 'Terms & Conditions',
        iconName: 'list_alt',
        routePath: '/mqp/terms',
      },
    ],
  },
];

@Injectable()
export class SideNavService {
  getMenuitem(): NavItem[] {
    return MENUITEMS;
  }
}
