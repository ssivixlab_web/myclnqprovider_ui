// Sidebar nav
export interface NavItem {
  id: number;
  displayName: string;
  iconName?: string;
  routePath?: string;
  children?: NavItem[];
}
